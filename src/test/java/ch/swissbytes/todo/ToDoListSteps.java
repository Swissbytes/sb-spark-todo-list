package ch.swissbytes.todo;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Pending;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

public class ToDoListSteps {
    private WebDriver driver;
    private ToDoList app = new ToDoList();

    public ToDoListSteps() {
        app.startup(new EntityManager());
        driver = new FirefoxDriver();
    }

    public void shutdown() {
        app.shutdown();
    }

    @Given("that I open the todo list site")
    public void givenThatIOpenTheTodoListSite() {
        driver.get("http://localhost:4567/items");
    }

    @Then("I should see $count items")
    public void thenIShouldSeeNItems(int count) {
        List<WebElement> items = driver.findElements(By.cssSelector("#items li"));
        assertThat(items.size(), is(count));
    }

    @When("I add a new item with title \"$title\" and description \"$description\"")
    public void whenIAddANewItem(String title, String description) {
        WebElement newItemLink = driver.findElement(By.linkText("New Item"));
        newItemLink.click();
        Assert.assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("Create new item"));

        WebElement titleField = driver.findElement(By.name("title"));
        WebElement descriptionField = driver.findElement(By.name("description"));
        titleField.sendKeys(title);
        descriptionField.sendKeys(description);
        titleField.submit();

        Assert.assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("Item added successfully!!"));
        WebElement backLink = driver.findElement(By.linkText("Back to list"));
        backLink.click();

        Assert.assertThat(driver.findElement(By.cssSelector("body")).getText(), containsString("My ToDo List"));
    }

    @Then("I complete the item with title \"$title\"")
    public void thenICompleteTheItemWithTitle(String title) {
        List<WebElement> items = driver.findElements(By.cssSelector("#items li"));
        WebElement newItem = null;
        for (WebElement item : items) {
            if (item.getText().contains(title)) {
                newItem = item;
            }
        }
        WebElement completeLink = newItem.findElement(By.className("complete"));
        completeLink.click();
    }
}
